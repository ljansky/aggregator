FROM ljansky/alpine-js-development:latest

COPY aggregator/package.json /usr/src/nm/aggregator/package.json

RUN cd /usr/src/nm && lerna bootstrap

RUN install-nm.sh

COPY aggregator/ /usr/src/out/aggregator/

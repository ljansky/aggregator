const FB = require('fb');

module.exports = async (config, formatter, resourceConfigCollection) => {

	return {
		async search () {

			const allResults = [];

			const storedConfig = await resourceConfigCollection.findOne({ resourceId: config.resourceId });
			if (storedConfig) {
				FB.setAccessToken(storedConfig.accessToken);

				const today = Math.round((new Date()).getTime() / 1000);
			

				for (let i in config.keywords) {
					const results = await FB.api('search', { type: config.fbType, q: config.keywords[i], limit: 100, since: today });
					results.data.forEach(item => {
						if (!allResults.find(i => i.id === item.id)) {
							allResults.push(item);
						}
					});
				}
			} else {
				console.log('NO ACCESS TOKEN');
			}

			return allResults.map(formatter);
		},

		async setToken (ctx) {
			const redirectUri = ctx.request.origin + '/set-token/' + config.resourceId;

			if (ctx.query.code) {
				const accessToken = await FB.api('oauth/access_token', {
					client_id: config.clientId,
					client_secret: config.clientSecret,
					redirect_uri: redirectUri,
					code: ctx.query.code
				});

				storedConfig = {
					resourceId: config.resourceId,
					accessToken: accessToken.access_token
				};

				const res = await resourceConfigCollection.update({ resourceId: config.resourceId }, storedConfig, { upsert: true });

				ctx.body = accessToken;

			} else {
				ctx.redirect('https://graph.facebook.com/oauth/authorize?redirect_uri=' + redirectUri + '&client_id=' + config.clientId);
			}
		}
	}
}
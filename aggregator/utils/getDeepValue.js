module.exports = (selector, obj) => {
	
	const selectorArray = selector.split('.');

	selectorArray.forEach(namePart => {
		if (typeof obj === 'undefined' || obj === null) {
			obj = null;
			return false;
		} else {
			obj = obj[namePart];
		}
	});

	return obj;
}

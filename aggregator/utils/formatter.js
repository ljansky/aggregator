const getDeepValue = require('./getDeepValue');

module.exports = format => {
	return input => {

		const output = {};

		for (let key in format) {
			const value = format[key].replace(/\{.*?\}/g, (match) => {
				const replacementKey = match.replace('{', '').replace('}', '');
				return getDeepValue(replacementKey, input) || '';
			});

			if (value && value !== '') {
				output[key] = value;
			}
		}

		return output;
	}
}
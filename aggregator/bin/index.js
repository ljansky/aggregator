const MongoClient = require('mongodb').MongoClient;
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const fs = require('fs');
const path = require('path');
const connectors = require('../connectors');
const FB = require('fb');
const getFormatter = require('../utils/formatter');

const app = new Koa();
const router = new Router();

async function runApp () {
	const client = await MongoClient.connect('mongodb://aggregator-mongo:27017/aggregator');
	const db = client.db('aggregator');
	const resourceConfigCollection = db.collection('resourceConfig');
	const itemsCollection = db.collection('items');
	const resources = {};

	const resourcePath = path.join(__dirname, '..', 'resource');
	const resourceConfigs = fs.readdirSync(resourcePath)
		.map(fileName => JSON.parse(fs.readFileSync(path.join(resourcePath, fileName), 'utf8')));

	for (let resourceConfig of resourceConfigs) {
		if (connectors[resourceConfig.type]) {
			resources[resourceConfig.resourceId] = await connectors[resourceConfig.type](resourceConfig, getFormatter(resourceConfig.format), resourceConfigCollection);
		}
	}

	router.get('/item', async (ctx, next) => {
		const items = await itemsCollection.find({
			status: {
				$in: ['new', 'updated']
			}
		}).toArray();

		ctx.body = items;
	});

	router.put('/item/:id/ignore', async (ctx, next) => {
		const id = ctx.params.id;
		ctx.body = await itemsCollection.update({ id }, { $set: { status: 'ignored' } });
	});

	router.put('/item/:id/process', async (ctx, next) => {
		const id = ctx.params.id;
		ctx.body = await itemsCollection.update({ id }, { $set: { status: 'processed' } });
	});

	router.get('/search', async (ctx, next) => {
		const updatedItems = [];
		for (let resourceId in resources) {
			const resourceItems = await resources[resourceId].search();
			const itemsMap = {};

			const existingItems = await itemsCollection.find({
				resourceId,
				status: { $not: /deleted/ }
			}).toArray();

			existingItems.forEach(item => {
				if (!resourceItems.find(i => i.id === item.id)) {
					updatedItems.push(Object.assign({}, item, { status: 'deleted' }));
				} else {
					itemsMap[item.id] = item;
				}
			});

			for (let item of resourceItems) {
				const existingItem = itemsMap[item.id];

				if (!existingItem) {
					updatedItems.push({ 
						id: item.id, 
						data: item, 
						modified: Date.now(),
						status: 'new',
						resourceId
					});

				} else if (JSON.stringify(item) !== JSON.stringify(existingItem.data)) {
					updatedItems.push(Object.assign({}, existingItem, {
						modified: Date.now(),
						data: item,
						status: existingItem.status === 'processed' ? 'updated' : existingItem.status
					}));
				}
			}
		}

		for (let item of updatedItems) {
			const updated = await itemsCollection.update({
				id: item.id,
				resourceId: item.resourceId,
				status: { $not: /deleted/ }
			}, item, { upsert: true });
		}

		ctx.body = updatedItems;
	});

	router.get('/set-token/:resourceId', async (ctx, next) => {
		const res = await resources[ctx.params.resourceId].setToken(ctx);
	});

	app
	  .use(router.routes())
	  .use(router.allowedMethods());
	 
	app.listen(3000);
}

runApp();